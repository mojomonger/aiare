'use client';

import { useChat } from 'ai/react';
import {useEffect, useRef} from "react";
import {systemPrompt_CssColors} from '../app/prompt_scripts/system.css_colors';

export default function Chat() {

    // https://sdk.vercel.ai/docs/api-reference/use-chat

    // call useChat with these various options:

    // api              useChat Defaults to `/api/chat`.
    //                  see the /api/chat/route.js file for the default behavior.
    //                  You can override this by passing a URL to useChat

    // initialInput		An optional string of initial prompt input
    // initialMessages  An optional array of initial chat messages

    // onResponse
    // onFinish

    // "helpers" are returned from useChat:

    /*
        messages:
            An array of chat messages that represents the current state of the chat.

        input:
            A string that represents the current text in the chat input field.

        handleInputChange:
            A function to handle changes to the input field, updating the input state.

        handleSubmit:
            A function to handle the form submission, which typically sends the current input to the chat backend and then clears the input field.
        append: A function to manually append a message to the chat, which could be useful for custom chat interactions or responses not directly tied to user input.
        reload: A function to reload the last AI chat response, useful in cases where the chat may need to pull the latest response from the backend again.
        stop: A function to stop the current chat interaction or API request, providing a way to halt operations if needed.
        setMessages: A function to manually set the entire messages array, allowing for more direct control over the chat state.
     */
    const { messages, input, handleInputChange, handleSubmit, isLoading, error,  } = useChat({
        api: "/api/chat",

        // initialInput: systemPrompt_CssColors,

        initialMessages: [
            {
                id: Date.now().toString(),
                role: 'system',
                content: systemPrompt_CssColors
            }
        ],
    });

    const scrollRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
        if (scrollRef.current === null) return
        scrollRef.current.scrollTo(0, scrollRef.current.scrollHeight)
    }, [messages])

    return (
        <main>

            <div className="chat-area">
                {/*<div className="flex flex-col w-full max-w-md py-24 mx-auto stretch">*/}

                <form onSubmit={handleSubmit}>
                    <input
                        className="fixed bottom-0 w-full max-w-md p-2 mb-8 border border-gray-300 rounded shadow-xl"
                        value={input}
                        placeholder="Enter prompt here..."
                        onChange={handleInputChange}
                    />
                </form>

                <div className={"message-container"}
                     style={{maxHeight:"23rem", border:"3px green", color:"purple", backgroundColor:"yellow"}}
                     ref={scrollRef}
                >
                    {messages.map(m => (
                        <div key={m.id} className="whitespace-pre-wrap">
                            {m.role === 'user' ? 'User: ' : 'AI: '}
                            {m.content}
                        </div>
                    ))}

                </div>

            </div>
        </main>
    );
}

