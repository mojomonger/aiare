'use client';

import React, {useEffect, useRef, useState} from "react";
// import { Tooltip } from 'geist/components';

import { useChat } from 'ai/react';
import { toast } from 'sonner'
import { SendHorizontalIcon, RefreshCcw, Zap } from 'lucide-react'

import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar'
import {ScrollArea} from "@/components/ui/scroll-area";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import CopyToClipboard from "@/components/copy-to-clipboard";
import CopyToClipboardText from "@/components/ui/copy-to-clipboard-text";
        // import {
        //     Tooltip,
        //     TooltipContent,
        //     TooltipProvider,
        //     TooltipTrigger,
        // } from "@/components/ui/tooltip"

import {CommitAction, Gitlab} from '@gitbeaker/rest';

import {fixBranchName} from "@/lib/utils";
import {contexts} from "@/lib/constants"
import {systemPrompt_CssColors} from '@/app/prompt_scripts/system.css_colors';


export default function NewChat() {

    const gitlabHost = 'https://gitlab.com'
    const gitProjectId = '57430392'  //gitlab.com/mojomonger/gptiare
    const gitProjectToken = 'glpat-wVLtyuT_DrPbwg9GQtex'  // specific to project owner; shall be placed in .env file
    const gitCssFilename = "/src/__custom__/iare_custom_overrides.css"
    const vercelDeploymentPattern = 'https://gptiare-git-%%branch_name%%-mojomongers-projects.vercel.app'

    const [lastPrompt, setLastPrompt] = useState<string>("")
    const [lastResponse, setLastResponse] = useState<string>("")
    const [gitTestInput, setGitTestInput] = useState<string>("")
    const [gitTestResponse, setGitTestResponse] = useState<string>("")
    const [gitBranchName, setGitBranchName] = useState<string>("")
    const [deployments, setDeployments] = useState<any[]>([]);



    // https://sdk.vercel.ai/docs/api-reference/use-chat

    // call useChat with these various options:

    // api              useChat Defaults to `/api/chat`.
    //                  see the /api/chat/route.js file for the default behavior.
    //                  You can override this by passing a URL to useChat

    // initialInput		An optional string of initial prompt input
    // initialMessages  An optional array of initial chat messages

    // onResponse
    // onFinish

    // "helpers" are returned from useChat:

    /*
        messages:
            An array of chat messages that represents the current state of the chat.

        input:
            A string that represents the current text in the chat input field.

        handleInputChange:
            A function to handle changes to the input field, updating the input state.

        handleSubmit:
            A function to handle the form submission, which typically sends the current input to the chat backend and then clears the input field.
        append: A function to manually append a message to the chat, which could be useful for custom chat interactions or responses not directly tied to user input.
        reload: A function to reload the last AI chat response, useful in cases where the chat may need to pull the latest response from the backend again.
        stop: A function to stop the current chat interaction or API request, providing a way to halt operations if needed.
        setMessages: A function to manually set the entire messages array, allowing for more direct control over the chat state.
     */
    const { messages, input, handleInputChange, handleSubmit, isLoading, error, } = useChat({
        api: "/api/chat",  // setting explicitly even tho useChat Defaults to `/api/chat`.

        initialMessages: [
            {
                id: Date.now().toString(),
                role: 'system',
                content: systemPrompt_CssColors
            }
        ],

        onResponse: response => {
            if (!response.ok) {
                const status = response.status

                switch (status) {
                    // case 401:
                    //     openSignIn()
                    //     break
                    default:
                        toast.error(error?.message || 'Something went wrong!')
                        break
                }
            }

        },


        onFinish: message => {
            setLastResponse(message.content)
        }

    });

    const scrollRef = useRef<HTMLDivElement>(null)
    const responseRef = useRef<HTMLDivElement>(null)
    const gitResultsRef = useRef<HTMLDivElement>(null)

    const api = new Gitlab({
        host: gitlabHost,
        token: gitProjectToken
    });

    function refreshDeployments() {

        // TODO: get these from vercel deployments instead;
        //  - maybe sort or filter
        api.Branches.all(gitProjectId).then((branches) => {

            console.log("branches:", branches);

            // @ts-ignore
            const branchList = branches.sort(
                // @ts-ignore
                (a, b) => (a?.commit?.committed_date > b?.commit?.committed_date)
                    ? -1
                    // @ts-ignore
                    : (a?.commit?.committed_date < b?.commit?.committed_date)
                        ? 1
                        : 0
            ).map(branch => branch.name)

            setDeployments(branchList)
        })
    }

    // for now, inserts deployment as a string into vercel pattern
    // later, the deployment may be a resolved vercel url
    function showDeployment(deployment:string) {
        const sanitizedDeployment = deployment.replace(/_/g, "")
        const vercelUrl = vercelDeploymentPattern.replace('%%branch_name%%', sanitizedDeployment)
        //alert(`Deployment: ${deployment} at ${vercelUrl}`)
        window.open(vercelUrl, '_blank')
    }


    function onSubmitTest() {
        alert(`Test git with input: ${gitTestInput}`)

        // api.Projects.show(gitProjectId).then((project) => {
        //     console.log(project);
        //     setGitTestResponse(JSON.stringify(project, null, 2))
        // });

        // api.Branches.all(gitProjectId).then((branches) => {
        //
        //     const branchList = branches.map(branch => {
        //         return branch.name
        //     })
        //     console.log(branches);
        //     setGitTestResponse(JSON.stringify(branchList, null, 2))
        // })
    }


    // allows authorization or other tasks to be performed before form submission
    //
    function onSubmitChat(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        // check for login here, or validate info...?
        handleSubmit(e)
    }

    async function onSubmitDeploy(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()

        // NB TODO use formal form verification
        //  - for now just do simple check of gitBranchName, which is controlled input,
        //  - so, maybe form validation is not appropriate here...

        const result = await createBranchAndUpdateFile(gitBranchName, lastResponse)

        setGitTestResponse(JSON.stringify(result, null, 2))

        if (!result.error) refreshDeployments()

    }

    async function createBranchAndUpdateFile(newBranchName:string, fileContents:string) {

        if (!fileContents) {
            alert("File contents (Last Response) must not be empty")
            return { error: true, error_message: "File contents must not be empty"}
        }

        if (!newBranchName) {
            alert("Branch name is required")
            return { error: true, error_message: "Branch name is required"}
        }

        const branchName = fixBranchName(newBranchName)
        // see if branch exists already...
        const branchExists = deployments.includes(branchName)
        if (branchExists) {
            alert("Branch already exists")
            return { error: true, error_message: "Branch already exists"}
        }

        const projectId = gitProjectId;
        // const newBranchName = 'new-feature';
        const sourceBranch = 'main';
        const filePath = gitCssFilename;
        // const fileContent = 'Updated content of the file';
        const commitMessage = 'Update CSS file with custom contents';

        const actions: CommitAction[] = [{
            action: 'update',
            filePath: filePath,
            content: fileContents
        }]

        try {
            // Create a new branch from the existing branch
            const branch = await api.Branches.create(projectId, branchName, sourceBranch);
            console.log('Branch created:', branch.name);

            // Create a commit on the new branch to update a file
            const commit = await api.Commits.create(
                projectId, branch.name, commitMessage,
                actions);

            console.log('File updated on new branch:', commit);

            return commit

        } catch (error) {
            console.error('Error during branch creation or file update:', error);
            return { error: true, error_message: "Error during branch creation or file update:" + error}
        }

    }

    // intercept prompt change so we can save it before passing on to useChat
    function onPromptChange(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault()

        // save value for our use
        setLastPrompt(e.target.value)

        // pass on to useChat event
        handleInputChange(e)
    }

    // intercept prompt change so we can save it before passing on to useChat
    function onBranchNameChange(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault()
        // save value for our use
        setGitBranchName(e.target.value)
    }

    // function onChangeGitTest(e: React.ChangeEvent<HTMLInputElement>) {
    //     e.preventDefault()
    //     // save value for our use
    //     setGitTestInput(e.target.value)
    // }

    useEffect(() => {
        if (scrollRef.current === null) return
        scrollRef.current.scrollTo(0, scrollRef.current.scrollHeight)
    }, [messages])

    useEffect(() => {
        if (responseRef.current === null) return
        responseRef.current.scrollTo(0, responseRef.current.scrollHeight)
    }, [lastResponse])

    useEffect(() => {
        refreshDeployments()
    }, [])

    return (
        <section className='py-8 text-zinc-700 bg-orange-100'>

            <div className='container max-w-6xl'>
                {/* Title section */}
                <div className='flex max-w-lg items-center justify-between px-1 pb-4 -ml-0.5'>
                    <h1 className='font-serif text-2xl font-medium'>AIARE Chatbot</h1>
                </div>

                <div className="grid grid-cols-6 gap-4">

                    {/* Column 1 */}

                    <div className="col-start-1 col-end-4">

                        <div className='font-serif text-xl font-medium border-b-2 border-neutral-700 mb-3 w-full'
                        >Create</div>

                        <div className="grid grid-cols-2 gap-4">

                            {/* Programming Code Context */}
                            <div className="mb-4">
                                <label htmlFor="code-context" className="mb-2 block text-sm font-medium">
                                    Context
                                </label>
                                <div className="relative">
                                    <select
                                        id="code-context"
                                        name="code-context"
                                        className="peer block w-full cursor-pointer rounded-md border border-gray-200 py-2 pl-2 text-sm outline-2 placeholder:text-gray-500"
                                        defaultValue="css_colors"
                                        // aria-describedby="context-error"
                                    >
                                        <option value="" disabled>
                                            Select a context
                                        </option>
                                        {contexts.map((context) => (
                                            <option key={context.id} value={context.id}>
                                                {context.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>

                            {/* AI Model selection */}
                            <div className="mb-4">
                                <label htmlFor="customer" className="mb-2 block text-sm font-medium">
                                    AI Model
                                </label>
                                <div className="relative">
                                    <select
                                        id="code-context"
                                        name="code-context"
                                        className="peer block w-full cursor-pointer rounded-md border border-gray-200 py-2 pl-2 text-sm outline-2 placeholder:text-gray-500"
                                        defaultValue="openai"
                                        // aria-describedby="context-error"
                                    >
                                        <option value="" disabled>
                                            Select model to use
                                        </option>
                                        {[{id:"openai", name:"OpenAI"}].map((context) => (
                                            <option key={context.id} value={context.id} >
                                                {context.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>

                        </div>

                        {/* Prompt Submission */}

                        <form onSubmit={onSubmitChat} className='relative'>
                            <label htmlFor="message" className="mb-2 block text-sm font-medium">
                                Enter a prompt that says what you want and click Submit.
                            </label>
                            <div className="relative">

                                <div className="mb-4">
                                    <Input
                                        name='message'
                                        value={input}
                                        onChange={onPromptChange}
                                        // onChange={handleInputChange}
                                        placeholder={"Type something..."}
                                        className='pr-12 placeholder:italic placeholder:text-zinc-600/75 focus-visible:ring-zinc-500'
                                    />
                                                {/*<Button*/}
                                                {/*    size='icon'*/}
                                                {/*    type='submit'*/}
                                                {/*    variant='secondary'*/}
                                                {/*    disabled={isLoading}  // responds to isLoading from chat response*/}
                                                {/*    className='absolute right-1 top-1 h-8 w-10'*/}
                                                {/*>*/}
                                                {/*    <SendHorizontalIcon className='h-5 w-5 text-emerald-500'/>*/}
                                                {/*</Button>*/}
                                    <Button
                                        className='absolute right-1 top-1 h-8
                                                bg-blue-500 text-white px-4 py-1 rounded-md
                                                hover:bg-blue-600 transition-colors
                                                focus-visible:outline focus-visible:outline-2
                                                focus-visible:outline-offset-2 focus-visible:outline-blue-600'
                                        // NB: these commented values are for another technique of buttonizing
                                        // size='icon'
                                        // type='submit'
                                        // variant='secondary'
                                        // disabled={false}
                                        // className='absolute right-1 top-1 h-12 w-10'
                                    >
                                        <span>Submit</span>
                                    </Button>
                                </div>
                            </div>
                        </form>

                        {/* Chat history */}
                        <div className='mx-auto mt-3 w-full max-w-2xl'>
                            <label htmlFor="chat-area" className="mb-2 block text-sm font-medium">
                                Chat History
                            </label>
                            <ScrollArea
                                className='mb-2 h-[355px] rounded-md border p-4 pr-0 bg-white'
                                ref={scrollRef}
                                id={"chat-area"}
                            >
                                {messages.map(m => (
                                    <div key={m.id} className='mr-6 whitespace-pre-wrap md:mr-12'>
                                        {m.role === 'user' && (
                                            <div className='mb-6 flex gap-3'>
                                                <Avatar>
                                                    <AvatarImage src=''/>
                                                    <AvatarFallback className='text-sm'>U</AvatarFallback>
                                                </Avatar>
                                                <div className='mt-1.5'>
                                                    <p className='font-semibold'>You</p>
                                                    <div className='mt-1.5 text-sm text-zinc-500'>
                                                        {m.content}
                                                    </div>
                                                </div>
                                            </div>
                                        )}

                                        {m.role === 'assistant' && (
                                            <div className='mb-6 flex gap-3'>
                                                <Avatar>
                                                    <AvatarImage src=''/>
                                                    <AvatarFallback className='bg-emerald-500 text-white'>
                                                        AI
                                                    </AvatarFallback>
                                                </Avatar>
                                                <div className='mt-1.5 w-full'>
                                                    <div className='flex justify-between'>
                                                        <p className='font-semibold'>Bot</p>
                                                        <CopyToClipboard message={m} className='-mt-1'/>
                                                    </div>
                                                    <div className='mt-2 text-sm text-zinc-500'>
                                                        {m.content}
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                ))}
                            </ScrollArea>

                        </div>

                    </div>

                    {/* Column 2 */}

                    <div className="col-start-4 col-end-7">

                        <div className='font-serif text-xl font-medium border-b-2 border-neutral-700 mb-3 w-full'
                        >Deploy</div>

                        <div className="grid grid-cols-2 gap-4">

                            {/* col 1 top area */}
                            <div>
                                {/* Last Prompt area */}
                                <div className="mb-4">
                                    <label htmlFor="customer" className="mb-2 block text-sm font-medium">
                                        Last Prompt
                                    </label>
                                    <div className={"relative"}>
                                        <div className='flex h-9 w-full px-3 py-1.5 rounded-md border border-input bg-neutral-200 text-sm pr-12'
                                        >{lastPrompt}</div>

                                        {/*<TooltipProvider>*/}
                                        {/*    <Tooltip>*/}
                                        {/*        <TooltipTrigger>*/}

                                        {/*            <Button*/}
                                        {/*                size='icon'*/}
                                        {/*                type='submit'*/}
                                        {/*                variant='secondary'*/}
                                        {/*                disabled={isLoading}  // responds to isLoading from chat response*/}
                                        {/*                className='absolute right-1 top-1 h-7 w-8'*/}
                                        {/*                onClick={() => {setGitBranchName(lastPrompt)}}  // copy last prompt to branch name*/}
                                        {/*            >*/}
                                        {/*                <SendHorizontalIcon className='h-5 w-5 text-blue-400'/>*/}
                                        {/*            </Button>*/}

                                        {/*        </TooltipTrigger>*/}
                                        {/*        <TooltipContent>*/}
                                        {/*            <p>Copy Last Prompt to Branch name</p>*/}
                                        {/*        </TooltipContent>*/}
                                        {/*    </Tooltip>*/}
                                        {/*</TooltipProvider>*/}

                                            {/*<Tooltip position="top" text="Copy Last Prompt to Branch Name">*/}
                                                <Button
                                                    size='icon'
                                                    type='submit'
                                                    variant='secondary'
                                                    disabled={isLoading}  // responds to isLoading from chat response
                                                    className='absolute right-1 top-1 h-7 w-8'
                                                    onClick={() => {setGitBranchName(lastPrompt)}}  // copy last prompt to branch name
                                                >
                                                    <SendHorizontalIcon className='h-5 w-5 text-blue-400'/>
                                                </Button>

                                            {/*</Tooltip>*/}

                                    </div>

                                </div>


                                {/* Last Response area */}
                                <div className='mx-auto mt-3 w-full max-w-2xl'>
                                    <div className='flex justify-between'>
                                        <label htmlFor="chat-area" className="mb-2 block text-sm font-medium">
                                            Last Response
                                        </label>
                                        <CopyToClipboardText message={lastResponse} className='-mt-1'/>
                                    </div>
                                    <ScrollArea
                                        className='mb-2 h-[300px] rounded-md border p-4 bg-neutral-200'
                                        ref={responseRef}
                                        id={"last-response"}
                                    >
                                        {/*<div className='mr-6 whitespace-pre-wrap md:mr-12'>*/}
                                        <div className='whitespace-pre-wrap xxmd:mr-12'>
                                            <div className='mb-6 flex gap-3'>
                                                <div className='mt-1.5'>
                                                    <div className='font-mono mt-1.5 text-xxs text-zinc-500'>
                                                        {lastResponse}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </ScrollArea>

                                </div>

                            </div>

                            {/* col 2 top area */}
                            <div>
                                {/* GitLab Branch Deploy */}
                                <form onSubmit={onSubmitDeploy} className='relative'>
                                    <label htmlFor="message" className="mb-2 block text-sm font-medium">
                                        New Branch Name
                                    </label>
                                    <div className="relative">

                                        <div className="mb-4">
                                            <Input
                                                name='message'
                                                value={gitBranchName}
                                                onChange={onBranchNameChange}
                                                placeholder={"Enter branch name..."}
                                                className='pr-12 placeholder:italic placeholder:text-zinc-600/75 focus-visible:ring-zinc-500'
                                            />
                                            <Button
                                                className='absolute right-1 top-1 h-8
                                                bg-blue-500 text-white px-4 py-1 rounded-md
                                                hover:bg-blue-600 transition-colors
                                                focus-visible:outline focus-visible:outline-2
                                                focus-visible:outline-offset-2 focus-visible:outline-blue-600'
                                                        // NB: these commented values are for another technique of buttonizing
                                                        // size='icon'
                                                        // type='submit'
                                                        // variant='secondary'
                                                        // disabled={false}
                                                        // className='absolute right-1 top-1 h-12 w-10'
                                            >
                                                <span>Deploy</span>
                                            </Button>
                                        </div>
                                    </div>
                                </form>

                                {/* Gitlab Past Deployments */}
                                <div className="mb-4">
                                    <div className='flex justify-between'>
                                        <label htmlFor="customer" className="mb-2 block text-sm font-medium">
                                            Deployed Branches
                                        </label>
                                        <Button
                                            size='icon'
                                            type='submit'
                                            variant='secondary'
                                            disabled={isLoading}  // responds to isLoading from chat response
                                            className='xxabsolute xxright-1 xxtop-1 xxmb-1 h-7 w-8 bg-transparent'
                                            onClick={() => {refreshDeployments()}}  // refresh deployment list
                                        >
                                            <RefreshCcw className='h-5 w-5 text-neutral-400'/>
                                        </Button>
                                    </div>
                                    <ScrollArea
                                        className='mb-2 h-[300px] rounded-md border p-2 bg-white'
                                        // ref={deploymentsRef}
                                        id={"past-deployments"}
                                    >

                                        {/*<div className='flex'>*/}

                                        <div className='mb-6 flex flex-col gap-1'>
                                            {deployments.map((deployment, index) => {
                                                return <Button key={deployment}
                                                    className='h-8
                                                    bg-blue-500 text-white px-4 py-1 rounded-md
                                                    hover:bg-blue-600 transition-colors
                                                    focus-visible:outline focus-visible:outline-2
                                                    focus-visible:outline-offset-2 focus-visible:outline-blue-600'

                                                    onClick={() => {
                                                        showDeployment(deployment)
                                                    }}
                                                >
                                                    <span>{deployment}</span>
                                                </Button>

                                            })}

                                        {/*</div>*/}
                                        </div>


                                    </ScrollArea>
                                </div>
                            </div>
                        </div>

                        {/* Git test area */}
                        <div>
                            {/* Results / Messages */}

                            <div className='flex justify-between'>
                                <label htmlFor="git-test-results" className="mb-2 block text-sm font-medium">
                                    Results / Error messages
                                </label>
                                <CopyToClipboardText message={gitTestResponse} className='-mt-1'/>
                            </div>
                            <ScrollArea
                                className='mb-2 h-[100px] rounded-md border p-4 bg-white'
                                ref={gitResultsRef}
                                id={"git-test-results"}
                            >
                                <div className='whitespace-pre-wrap xxxmd:mr-12'>
                                    <div className='mb-6 flex gap-3'>
                                        <div className='mt-1.5'>
                                            <div className='font-mono mt-1.5 text-xxs text-zinc-500'>
                                                {gitTestResponse}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </ScrollArea>
                        </div>  {/* end git test area */}

                    </div>  {/* end page column 2 */}

                </div>  {/* end page grid */}

            </div>  {/* end container */}

        </section>
    )
}



