
## Instructions

1. Enter a prompt phrase to indicate what you want
the AI to generate and click submit.
   * The "Context" field is for specifying what kind of information will be generated. For now, only CSS Color Scheme is suported.
   * The "AI Model" field is for specifying which AI Model to use when generating responses. For now, only "Open AI" is supported.
 
2. After the AI generates its response, enter a Branch Name and click 
"Deploy". This will generate a new code branch with the newly generated AI content.
   * You can click the right arrow button in the "Last Prompt" text area  to copy the Last Prompt 
phrase into the "New Branch Name" field.
3. The new branch should be generated, and a button appears in the
"Deployed Branches" list withe name of the branch specified. Click on the button to see your newly generated program!

## Caveats

* Branch names will remove underscores and spaces in the final URL. 
It is best to avoid spaces and underscores for now.
* If the new branch does not appear in the "Deployed Branches" list
immediately, click on the "refresh" icon at the top right of the
"Deployed Branches" area.
* All branches in the "Deployed Branches" list may not point to an active 
deployment. This is because the auto-deploy process was not in place when the branch was created. 
