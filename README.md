This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Development site set up for deploymrent at: localhost:3011

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```
The port can be changed via the "-p {PORT}" arguments to the "next dev"command in the
scripts section of package.json. 

The port is currently set to 3011 for this app.

```
{
"name": "my-ai-app",
"version": "0.1.0",
"private": true,
"scripts": {
    "dev": "next dev -p 3011",
    "build": "next build",
    "start": "next start",
    "lint": "next lint"
},
...
```

Open [http://localhost:3011](http://localhost:3011) with your browser to see the result.

As you edit files, the hosted development page auto-updates as you edit the files.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Token Authentication

GitLab token expires after 1 year.make new one and update the .env.local file with the new token.
(eventually --- env.local not implemnted yet)

## Shadcn UI

This app makes use of the Shadcn UI components:

https://ui.shadcn.com/docs/components


## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
