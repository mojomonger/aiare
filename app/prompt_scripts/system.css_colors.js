export const systemPrompt_CssColors = `
I want a CSS definition file for the elements specified in the format attached.
The new color values should match the original in brightness and saturation.
Make sure the body-bg is changed to something different from the original.
The file should be able to be easily edited with any text editor.
The file should be saved as a CSS file.
Return just the code snippet.
Do not include css string literal
Do not include the file format description
Please avoid using the abbreviation for Cascading Style Sheets and the common markdown syntax for blocks of code.

=====
File format:

:root {
    --g-color-h1-fg: red;

    --g-color-body-bg: #FFFFFF;
Ω
    --g-color-list-header-fg: black;
    --g-color-list-header-bg: #cff0ff;/*#00eaff;*//*#26e3f5;*/

    --g-color-function-box-caption-fg: #FFFFFF;
    --g-color-function-box-caption-bg: #2299e8;
    --g-color-function-box-caption-outline: #2585cc;
    --g-color-function-box-contents: hsl(228 23% 97% / 1);

    --g-color-flock-box-caption-fg: #FFFFFF;
    --g-color-flock-box-caption-bg: #2299e8;

    --g-color-reference-display-bg: hsl(211deg 65.22% 81.96%);
    --g-color-reference-display-fg: black;
    --g-color-reference-display-selected-bg: #e3ebf9;

}
`;

/*
Don’t justify your answers. Don’t give information not mentioned in the CONTEXT INFORMATION.

https://community.openai.com/t/how-to-prevent-chatgpt-from-answering-questions-that-are-outside-the-scope-of-the-provided-context-in-the-system-role-message/112027

 */
// export const systemPrompt_CssColors = `
// I want a CSS definition file for the elements specified in the format attached.
// The new color values should match the original in brightness and saturation.
// The file should be able to be easily edited with any text editor.
// The file should be saved as a CSS file.
// Return just the code snippet, not a full file.
//
// Don’t justify your answers. Don’t give information not mentioned in the CONTEXT INFORMATION.
//
// =====
//
// File format:
//
// :root {
//     --g-color-h1-fg: red;
//
//     --g-color-body-bg: #FFFFFF;
// Ω
//     --g-color-list-header-fg: black;
//     --g-color-list-header-bg: #cff0ff;/*#00eaff;*//*#26e3f5;*/
//
//     --g-color-function-box-caption-fg: #FFFFFF;
//     --g-color-function-box-caption-bg: #2299e8;
//     --g-color-function-box-caption-outline: #2585cc;
//     --g-color-function-box-contents: hsl(228 23% 97% / 1);
//
//     --g-color-flock-box-caption-fg: #FFFFFF;
//     --g-color-flock-box-caption-bg: #2299e8;
//
//     --g-color-reference-display-bg: hsl(211deg 65.22% 81.96%);
//     --g-color-reference-display-fg: black;
//     --g-color-reference-display-selected-bg: #e3ebf9;
//
// }
// `;